var mainTests = require('./mainTests');
var RenderUrlsToFile, arrayOfUrls, system;

var system = require("system");
var fs = require('fs');
//var inputFileName = 'urls.csv';
var inputFileName = 'urls_1.csv';
//var inputFileName = 'urls_2.csv';
var fullDataFile = 'output-full.csv';
var resultsOnlyFile = 'output.csv'
var outputToCSV = {};

var test = mainTests.tests();
var httpTests = test.getHTTPTests();
var variables = test.getVariablesToTest();
var datalayer = null;

var pageEvaluateDelay = 3000;
var currentPageEvaluateDelay = 3000;

//Read URLs
arrayOfUrls = [];
var file_h = fs.open(inputFileName, 'r');
var line = file_h.readLine();
while (line) {
    if (line !=='')
        arrayOfUrls.push(line);
    line = file_h.readLine();
}
file_h.close();
/*
 @param array of URLs to render
 @param callbackPerUrl Function called after finishing each URL, including the last URL
 @param callbackFinal Function called after finishing everything
 */
RenderUrlsToFile = function (urls, callbackPerUrl, callbackFinal) {
    var getFilename, next, page, retrieve, urlIndex, webpage;
    urlIndex = 0;
    webpage = require("webpage");
    page = null;
    var asyncResult = {};
    //tmp url in case it needs to be re-executed;
    var currentURL = '';
    var totalURLs = urls.length;
    var iterations = 0;

    next = function (status, url, file) {
        page.close();
        if ((status !== 'success' || typeof(asyncResult[url]['adobe'])==='undefined' ) && url!==''){
            //In case a page did not load correctly, retry and push the URL at the end of the URL array.
            console.log('Failed URL: ' + url);
            urls.push(url);
    //        return retrieve();
        } else 
            callbackPerUrl(status, url, file, asyncResult);
        
        return retrieve();
    };
    retrieve = function () {
        var url;
        if (urls.length > 0) {
            currentPageEvaluateDelay = pageEvaluateDelay * Math.floor((totalURLs + urlIndex) / totalURLs);
            console.log('Delay: ' + currentPageEvaluateDelay);
            url = urls.shift();
            urlIndex++;
            page = webpage.create();
            console.log('Current URL <' + url + '> - ' + urlIndex + ' - URLs left: ' + urls.length);
            asyncResult[url] = {};
            // page.onError = function (msg, trace) {
            //     //console.log(msg);
            //     trace.forEach(function (item) {
            //         //console.log('  ', item.file, ':', item.line);
            //     });
            // };

            page.onResourceReceived = function (response) {
                httpTests.forEach(function (entry) {
                    var key = entry.key;
                    var marketingUrl = entry.url;
                    if (response['url'].indexOf(marketingUrl) > -1 && response.stage === 'end' && response['status'] === 200) {
                        //Overwrite variable only if it not defined (first run) or success. Otherwise it will keep the error code for the tag.
                        if ((typeof(asyncResult[url][key]) === 'undefined')) {
                            asyncResult[url][key] = 'Ok';
                        }
                            
                    }
                });
            };

            //page.settings.userAgent = 'BSQ_TEST';
            // page.onResourceRequested  = function (requestData, networkRequest) {
            //     httpTests.forEach(function (entry) {
            //         var key = entry.key;
            //         var marketingUrl = entry.url;

            //         //console.log(JSON.stringify(networkRequest));
            //         if (requestData['url'].indexOf(marketingUrl) > -1) {
            //             //Overwrite variable only if it not defined (first run) or success. Otherwise it will keep the error code for the tag.
            //             //if (typeof (asyncResult[url][key]) === 'undefined')
            //                 //asyncResult[url][key] = 'Ok';

            //             if (requestData['url'].indexOf('sonyglobal.d1.sc.omtrdc.net') >-1 ){
            //                 console.log('Omniture Captured');
            //                 //networkRequest.abort();
            //             }
            //         }
            //     });
            // };
            // page.onLoadFinished = function (status) {
            //     httpTests.forEach(function (entry) {
            //         var key = entry.key;
            //         if (typeof (asyncResult[key]) === 'undefined') {
            //             asyncResult[key] = 'Failed';
            //         }
            //     });
            // };

            var urlToOpen = url;
            if (urlToOpen.indexOf('http://') === -1)
                urlToOpen = "http://" + urlToOpen;
            //Open page - Create sandboxed environment to evaluate in-page variables.
            return page.open(urlToOpen, function (status) {
                var file;
                //file = getFilename();
                if (status === "success") {
                    return window.setTimeout((function () {
                        //Function to be executed inside the page that is opened.
                        var pageEvaluate = function (vars) {
                            //Query string extraction
                            var QueryString = function() {
                                var e = {};
                                var _location = (typeof _originalLocation !== "undefined" && typeof _originalLocation.search !== "undefined") ? _originalLocation : window.location;
                                var _search = _location.search || "";
                                var t = _search.substring(1);
                                var n = t.split("&");
                                for (var r = 0; r < n.length; r++) {
                                  var i = n[r].split("=");
                                  if (typeof e[i[0]] === "undefined") {
                                    e[i[0]] = i[1];
                                  } else if (typeof e[i[0]] === "string") {
                                    var s = [e[i[0]], i[1]];
                                    e[i[0]] = s;
                                  } else {
                                    e[i[0]].push(i[1]);
                                  }
                                }
                                return e;
                            }();

                            var outputToCSV = {};
                            var errors = 'No Errors During loading';
                            try {
                                outputToCSV.dataLayer = new SonyAnalytics().build().analytics;
                                outputToCSV.dataLayer.domain = window.location.host;
                                outputToCSV.dataLayer.queryStringObject = QueryString;
                                outputToCSV.pageName = s.pageName ? s.pageName : '';
                                //outputToCSV.s_utpageName = s_ut.pageName ? s_ut.pageName : '';
                            } catch (e) {
                                if (typeof (s) === 'undefined')
                                    s = {};
                                errors = 'Error during loading';
                                console.log(errors);
                            } finally {
                                var jsonObj = JSON.parse(vars);
                                outputToCSV.errors = errors;
                                jsonObj.forEach(function (property) {
                                    outputToCSV[property] = s[property] ? s[property] : '';
                                });
                            }
                            return outputToCSV;
                        };

                        //Evaluate custom JS inside the page.
                        var pageResults = page.evaluate(pageEvaluate, variables);
                        if (pageResults.errors.toString() === 'Error during loading') { //This Error message needs to match the message during page load. Line: 127
                            console.log('Error found during loading. Re-Trying..');
                            //set status failed to reset page view.
                            status = 'failed';
                        }
                        return next(status, url, pageResults);
                    }), currentPageEvaluateDelay);
                } else {
                    return next(status, url, 'No result');
                }
            });
        } else {
            return callbackFinal();
        }
    };
    return retrieve();
};

var callbackPerURLfunction = function (status, url, result, asyncResult) {
    if (status !== "success") {
        return console.log("URL:'" + url + " - Failed to open");
    } else {
        outputToCSV[url] = {};
        for (var property in result) {
            if (result.hasOwnProperty(property)) {
                outputToCSV[url][property] = result[property];
                //console.log(result[property]);
            }
        }
        console.log(JSON.stringify(asyncResult[url]));
        for (var property in asyncResult[url]) {
            if (asyncResult[url].hasOwnProperty(property)) {
                outputToCSV[url][property] = asyncResult[url][property];
            }
        }
        return console.log("URL: " + url + " - Results: " + status);
    }
};

var finalCallback = function () {
    var finalOutput = '';
    var finalOutputJSON = [];
    var headerOutput = '';
    var firstIteration = true;
    var delimiter = ';';
    var counter = 0;
    for (var url in outputToCSV) {
        dataLayer = outputToCSV[url].dataLayer;
        if (outputToCSV.hasOwnProperty(url)) {
            //console.log (JSON.stringify(outputToCSV[url]));
            headerOutput += 'URL' + delimiter;
            finalOutput += url + delimiter;
            finalOutputJSON[counter] = {};
            finalOutputJSON[counter].url = url;
            for (var subproperty in outputToCSV[url]) {
                if (subproperty.toString() !== 'dataLayer') {
                    if (firstIteration) {
                        headerOutput += subproperty.toString() + delimiter;
                        headerOutput += 'Status' + delimiter;
                    }
                    var validationResult = test.ValidateProperty(subproperty.toString(), outputToCSV[url][subproperty], dataLayer);
                    finalOutputJSON[counter][subproperty] = outputToCSV[url][subproperty];
                    if (validationResult === false){
                        console.log('Validation Error: ' + url + ' - ' + subproperty + ' - ' + outputToCSV[url][subproperty]);
                    }
                    //console.log((typeof(finalOutputJSON[counter]['result'])!=='undefined' && finalOutputJSON[counter]['result'].indexOf('Validation Error') === -1));
                    //console.log(finalOutputJSON[counter]['result'].indexOf('Validation Error') === -1));
                    finalOutputJSON[counter]['result'] = (typeof(finalOutputJSON[counter]['result'])==='undefined') ? '' : finalOutputJSON[counter]['result'];
                    if (validationResult === false) 
                        finalOutputJSON[counter]['result'] = finalOutputJSON[counter]['result'] + ' / ' + subproperty;
                    //console.log(finalOutputJSON[counter]['result']);
                    finalOutput += outputToCSV[url][subproperty] + delimiter;
                    finalOutput += validationResult + delimiter;
                }
            }
            finalOutputJSON[counter]['result'] = (finalOutputJSON[counter]['result'] === '') ? 'Validation Passed' : finalOutputJSON[counter]['result'];
            if (firstIteration) {
                headerOutput += '\n';
                finalOutput = headerOutput + finalOutput;
            }
            firstIteration = false;
        }
        finalOutput += '\n';
        counter += 1;
    }
    console.log('Total Results: ' + counter);
    try {
        fs.write(fullDataFile , finalOutput , 'w');
        fs.write(resultsOnlyFile , JSON.stringify(finalOutputJSON), 'w');
    } catch (e) {
        console.log(e);
    }
    return phantom.exit();
};

RenderUrlsToFile(arrayOfUrls, callbackPerURLfunction, finalCallback);