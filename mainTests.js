var marketingTagsURLs = [
    {key: 'adobe', url: 'sonyglobal.d1.sc.omtrdc.net/b/ss/'},
    {key: 'nexus', url: 'nexus.ensighten.com/sonyglobal/prod/'}//,
    //{key: 'googleAnalytics', url : 'google-analytics.com/r/collect'}
];

var validateCountry = function (countryCode, reportSuite, currencyCode) {

    var countries = [];
    countries["www.sony.com.ar"] = ["sonygwt-csa-prod", "ARS"];
    countries["www.sony.com.bo"] = ["sonygwt-csa-prod", "BOB"];
    countries["www.sony.com.br"] = ["sonygwt-br-prod", "BRL"];
    countries["www.sony.cl"] = ["sonygwt-csa-prod", "CLP"];
    countries["www.sony.com.co"] = ["sonygwt-csa-prod", "COP"];
    countries["www.sony.co.cr"] = ["sonygwt-csa-prod", "CRC"];
    countries["www.sony.com.do"] = ["sonygwt-csa-prod", "DOP"];
    countries["www.sony.com.ec"] = ["sonygwt-csa-prod", "USD"];
    countries["www.sony.com.sv"] = ["sonygwt-csa-prod", "USD"];
    countries["www.sony.com.gt"] = ["sonygwt-csa-prod", "GTQ"];
    countries["www.sony.com.hn"] = ["sonygwt-csa-prod", "HNL"];
    countries["www.sony.com.mx"] = ["sonygwt-csa-prod", "MXN"];
    countries["www.sony.com.ni"] = ["sonygwt-csa-prod", "NIO"];
    countries["www.sony.com.pa"] = ["sonygwt-csa-prod", "PAB"];
    countries["www.sony.com.pe"] = ["sonygwt-csa-prod", "PEN"];
    countries["www.sonypr.com"] = ["sonygwt-csa-prod", "USD"];
    countries["www.sony.com.pr"] = ["sonygwt-csa-prod", "USD"];
    countries["www.sony.co.uk"] = ["sonygwt-eu-prod", "GBP"];
    countries["www.sony.de"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.fr"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.it"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.ru"] = ["sonygwt-eu-prod", "RUB"];
    countries["www.sony.at"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.be"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.ba"] = ["sonygwt-eu-prod", "BAM"];
    countries["www.sony.bg"] = ["sonygwt-eu-prod", "BGN"];
    countries["www.sony.hr"] = ["sonygwt-eu-prod", "HRK"];
    countries["www.sony.gr"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.cz"] = ["sonygwt-eu-prod", "CZK"];
    countries["www.sony.dk"] = ["sonygwt-eu-prod", "DKK"];
    countries["www.sony.ee"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.fi"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.hu"] = ["sonygwt-eu-prod", "HUF"];
    countries["www.sony.ie"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.kz"] = ["sonygwt-eu-prod", "KZT"];
    countries["www.sonylatvija.com"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.lt"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.lu"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.com.mk"] = ["sonygwt-eu-prod", "MKD"];
    countries["www.sony.rs"] = ["sonygwt-eu-prod", "RSD"];
    countries["www.sony.ro"] = ["sonygwt-eu-prod", "RON"];
    countries["www.sony.nl"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.no"] = ["sonygwt-eu-prod", "NOK"];
    countries["www.sony.pt"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.sk"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.si"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.es"] = ["sonygwt-eu-prod", "EUR"];
    countries["www.sony.se"] = ["sonygwt-eu-prod", "SEK"];
    countries["www.sony.ch"] = ["sonygwt-eu-prod", "CHF"];
    countries["www.sony.com.tr"] = ["sonygwt-eu-prod", "TRY"];
    countries["www.sony.ua"] = ["sonygwt-eu-prod", "UAH"];

    if (typeof (countries[countryCode]) !== 'undefined') {
        if (typeof (reportSuite) !== 'undefined' && reportSuite !== null && typeof (countries[countryCode][0]) !== 'undefined')
            return countries[countryCode][0] === reportSuite;
        else if (typeof (currencyCode) !== 'undefined' && currencyCode !== null && typeof (countries[countryCode][1]) !== 'undefined')
            return countries[countryCode][1] === currencyCode;
    } else {
        return false;
    }
};
var validateReportSuite = function (value, dataLayer) {
    var countryCode = dataLayer.page.country;
    var domain = dataLayer.domain;
    return validateCountry(domain, value, null);
};
var validatePageName = function (value, dataLayer) {
    //console.log('Inside validation: ' + JSON.stringify(dataLayer.queryStringObject));
    var QueryString = dataLayer.queryStringObject;
    var tmp_pageName = value;
    //var dl = new SonyAnalytics().build().analytics;
    var dl = dataLayer;
    function gallery_sanitizeFilters(urlparams, sep) {
        var filterParameters = ['bestfor', 'category', 'type', 'cameramount'];
        var fitlerString = '';
        var filterSeparator = ';';
        for (var i = 0; i < filterParameters.length; i++)
            if (urlparams.hasOwnProperty(filterParameters[i]))
                if (fitlerString.length > 0)
                    if (urlparams[filterParameters[i]] instanceof Array)
                        fitlerString += filterSeparator + filterParameters[i] + '\x3d' + urlparams[filterParameters[i]].sort().join(',');
                    else
                        fitlerString += filterSeparator + filterParameters[i] + '\x3d' + urlparams[filterParameters[i]].split(',').sort().join(',');
                else if (urlparams[filterParameters[i]] instanceof Array)
                    fitlerString = filterParameters[i] + '\x3d' + urlparams[filterParameters[i]].sort().join(',');
                else
                    fitlerString = filterParameters[i] + '\x3d' + urlparams[filterParameters[i]].split(',').sort().join(',');
        if (fitlerString.length > 0)
            return sep + fitlerString;
        else
            return '';
    }

    var localpagename;
    switch (dl.page.template) {
        case 'home':
            return tmp_pageName === dl.page.section + ':' + dl.page.name;
            break;
        case 'category':
            if (typeof (dl.product.category2) !== 'undefined' && typeof (dl.product.category1) !== 'undefined')
                return tmp_pageName === dl.page.section + ':' + dl.page.template + ':' + dl.product.category1 + ':' + dl.product.category2;
            else if (typeof (dl.product.category1) !== 'undefined')
                return tmp_pageName === dl.page.section + ':' + dl.page.template + ':' + dl.product.category1;
            break;
        case 'gallery':
            if (dl.page.layout === 'comparison')
                localpagename = dl.page.section + ':' + dl.page.template + ':compare:' + dl.product.category1 + ':' + dl.product.category2 + ':' + dl.page.tab_name;
            else
                localpagename = dl.page.section + ':' + dl.page.template + ':' + dl.product.category1 + ':' + dl.product.category2 + ':' + dl.page.tab_name;
            localpagename = localpagename + gallery_sanitizeFilters(QueryString, ' > ');
            return tmp_pageName === localpagename;
            break;
        case 'product_details':
            localpagename = dl.page.section + ':pdp:' + dl.product.category1 + ':' + dl.product.category2 + ':';
            if (typeof (dl.product.supermodel_ids[0]) !== 'undefined') {
                return tmp_pageName === localpagename + dl.product.supermodel_ids[0].toUpperCase();//.replace(' / ', ' ', 'g').replace(/[ /-]/g, '/', 'g');
            } else if (typeof (dl.product.model_ids[0]) !== 'undefined') {
                return tmp_pageName === localpagename + dl.product.model_ids[0].toUpperCase();//.replace(' / ', ' ', 'g').replace(/[ /-]/g, '/', 'g');
            } else if (typeof (dl.product.sku_ids[0]) !== 'undefined') {
                return tmp_pageName === localpagename + dl.product.sku_ids[0].toUpperCase();//.replace(' / ', ' ', 'g').replace(/[ /-]/g, '/', 'g');
            }
        case 'reviews_listing':
            localpagename = dl.page.section + ':' + dl.page.template + ':';
            if (typeof (dl.product.supermodel_ids[0]) !== 'undefined') {
                return tmp_pageName === localpagename + dl.product.supermodel_ids[0].toLowerCase().replace(' / ', ' ', 'g').replace(/[ /-]/g, '-', 'g');
            } else if (typeof (dl.product.model_ids[0]) !== 'undefined') {
                return tmp_pageName === localpagename + dl.product.model_ids[0].toLowerCase().replace(' / ', ' ', 'g').replace(/[ /-]/g, '-', 'g');
            } else if (typeof (dl.product.sku_ids[0]) !== 'undefined') {
                return tmp_pageName === localpagename + dl.product.sku_ids[0].toLowerCase().replace(' / ', ' ', 'g').replace(/[ /-]/g, '-', 'g');
            }
        case 'other':
            if (dl.page.template_revision === 'all-electronics')
                return tmp_pageName === dl.page.section + ':' + dl.page.name.replace('other:', '');
            else if (dl.page.template_revision === 'locale-selector')
                return tmp_pageName === dl.page.section + ':' + dl.page.template + ':locale_selector';
            else if (dl.page.template_revision === 'language-selection')
                return tmp_pageName === dl.page.section + ':language-selection';
            else if (dl.page.template_revision === 'error-default')
                return tmp_pageName === dl.page.section + ':error-404';
            else if (dl.page.template_revision === 'error_page')
                return tmp_pageName === dl.page.section + ':error-503';
            else if (dl.page.template_revision === 'favorites' || dl.page.template_revision.indexOf('Page-Favorites') > -1)
                return tmp_pageName === dl.page.section + ':my favorites';
            else if (dl.page.template_revision === 'search-results')
                return tmp_pageName === dl.page.section + ':search';
            else
                return tmp_pageName === dl.page.section + ':' + dl.page.name;
        case 'results':
            return tmp_pageName === dl.page.section + ':search';
        case 'out_of_flow':
            if (typeof (dl.gallery === 'undefined'))
                return tmp_pageName === dl.page.section + ':' + dl.page.name.replace('out_of_flow:', 'oof:').replace('_oof', '');
            else if (typeof (dl.gallery !== 'undefined'))
                return tmp_pageName === dl.page.section + ':' + dl.page.name.replace('out_of_flow:', 'seo:').replace('_oof', 'seo');
        case 'product_tech_specs':
            var tempName = dl.page.section + ':pdp-specs:' + dl.product.category1 + ':' + dl.product.category2;
            if (typeof (dl.product.supermodel_ids[0]) !== 'undefined')
                return tmp_pageName === tempName + ':' + dl.product.supermodel_ids[0].toUpperCase();
            else if (typeof (dl.product.model_ids[0]) !== 'undefined')
                return tmp_pageName === tempName + ':' + dl.product.model_ids[0].toUpperCase();
            else if (typeof (dl.product.sku_ids[0]) !== 'undefined')
                return tmp_pageName === tempName + ':' + dl.product.sku_ids[0].toUpperCase();
        case 'subgallery':

            if ((typeof (dl.product.category2) !== 'undefined' && typeof (dl.product.category1) !== 'undefined' && typeof (dl.page.tab_layout) === 'undefined'))
                return tmp_pageName === dl.page.section + ':' + dl.page.template + ':' + dl.product.category1 + ':' + dl.product.category2 + ':' + dl.page.tab_name;
            else if (typeof (dl.product.category2) !== 'undefined' && typeof (dl.product.category1) !== 'undefined' && typeof (dl.page.tab_layout) !== 'undefined' && dl.page.tab_layout === 'comparison')
                return tmp_pageName === dl.page.section + ':' + dl.page.template + ':compare:' + dl.product.category1 + ':' + dl.product.category2 + ':' + dl.page.tab_name;
    }
};
var validateProp23 = function (value, dataLayer) {
    //var dl = new SonyAnalytics().build().analytics;
    var QueryString = dataLayer.queryStringObject;
    var dl = dataLayer;
    var s = {};
    s.prop23 = value;
    function gallery_sanitizeFilters(urlparams, sep) {
        var filterParameters = ["bestfor", "category", "type", "cameramount"];
        var fitlerString = "";
        var filterSeparator = ";";
        for (var i = 0; i < filterParameters.length; i++)
            if (urlparams.hasOwnProperty(filterParameters[i]))
                if (fitlerString.length > 0)
                    if (urlparams[filterParameters[i]] instanceof Array)
                        fitlerString += filterSeparator + filterParameters[i] + "\x3d" + urlparams[filterParameters[i]].sort().join(",");
                    else
                        fitlerString += filterSeparator + filterParameters[i] + "\x3d" + urlparams[filterParameters[i]].split(",").sort().join(",");
                else if (urlparams[filterParameters[i]] instanceof Array)
                    fitlerString = filterParameters[i] + "\x3d" + urlparams[filterParameters[i]].sort().join(",");
                else
                    fitlerString = filterParameters[i] + "\x3d" + urlparams[filterParameters[i]].split(",").sort().join(",");
        if (fitlerString.length > 0)
            return sep + fitlerString;
        else
            return "";
    }
    ;
    var localpagename;
    switch (dl.page.template) {
        case 'home':
            return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.name;
            break;
        case 'category':
            if (typeof (dl.product.category2) !== 'undefined' && typeof (dl.product.category1) !== 'undefined')
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.template + ':' + dl.product.category1 + ':' + dl.product.category2;
            else if (typeof (dl.product.category1) !== 'undefined')
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.template + ':' + dl.product.category1;
            break;
        case 'gallery':
            if (dl.page.layout === 'comparison')
            {
                localpagename = dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.template + ':compare:' + dl.product.category1 + ':' + dl.product.category2 + ':' + dl.page.tab_name;
            }
            else
            {
                localpagename = dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.template + ':' + dl.product.category1 + ':' + dl.product.category2 + ':' + dl.page.tab_name;
            }
            return s.prop23 === localpagename + gallery_sanitizeFilters(QueryString, ' > ');
            break;
        case 'product_details':
            if (typeof (dl.product.supermodel_ids[0]) !== 'undefined') {
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':pdp:' + dl.product.category1 + ':' + dl.product.category2 + ':' + dl.product.supermodel_ids[0].toUpperCase();//.replace(' / ', ' ', 'g').replace(/[ /-]/g, '-', 'g');
            } else if (typeof (dl.product.model_ids[0]) !== 'undefined') {
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':pdp:' + dl.product.category1 + ':' + dl.product.category2 + ':' + dl.product.model_ids[0].toUpperCase();//.replace(' / ', ' ', 'g').replace(/[ /-]/g, '-', 'g');
            } else if (typeof (dl.product.sku_ids[0]) !== 'undefined') {
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':pdp:' + dl.product.category1 + ':' + dl.product.category2 + ':' + dl.product.sku_ids[0].toUpperCase();//.replace(' / ', ' ', 'g').replace(/[ /-]/g, '-', 'g');
            }
            break;
        case 'reviews_listing':
            if (typeof (dl.product.supermodel_ids[0]) !== 'undefined') {
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.template + ':' + dl.product.supermodel_ids[0].toLowerCase().replace(' / ', ' ', 'g').replace(/[ /-]/g, '-', 'g');
            } else if (typeof (dl.product.model_ids[0]) !== 'undefined') {
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.template + ':' + dl.product.model_ids[0].toLowerCase().replace(' / ', ' ', 'g').replace(/[ /-]/g, '-', 'g');
            } else if (typeof (dl.product.sku_ids[0]) !== 'undefined') {
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.template + ':' + dl.product.sku_ids[0].toLowerCase().replace(' / ', ' ', 'g').replace(/[ /-]/g, '-', 'g');
            }
            break;
        case 'other':
            if (dl.page.template_revision === 'all-electronics')
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.name.replace('other:', '');
            else if (dl.page.template_revision === 'locale-selector')
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.template + ':locale_selector';
            else if (dl.page.template_revision === 'language-selection')
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':language-selection';
            else if (dl.page.template_revision === 'error-default')
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':error-404';
            else if (dl.page.template_revision === 'error_page')
                return s.prop23 === dl.page.section + dl.page.country.toLowerCase() + ':error-503';
            else if (dl.page.template_revision === 'favorites' || dl.page.template_revision.indexOf("Page-Favorites") > -1)
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':my favorites';
            else if (dl.page.template_revision === 'search-results')
                return s.prop23 === dl.page.section + ':' +  dl.page.country.toLowerCase() + ':' + 'search';
            else
                return s.prop23 === dl.page.section + ':' + dl.page.name;
            break;
        case 'results':
            return s.prop23 === dl.page.section + ':search';
            break;
        case 'out_of_flow':
            if (typeof (dl.gallery === 'undefined'))
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.name.replace('out_of_flow:', 'oof:').replace('_oof', '');
            else if (typeof (dl.gallery !== 'undefined'))
                return s.prop23 === dl.page.section + ':' + dl.page.country.toLowerCase() + ':' + dl.page.name.replace('out_of_flow:', 'seo:').replace('_oof', 'seo');
            break;
        case 'product_tech_specs':
            var tempName = dl.page.section + ':' + dl.page.country.toLowerCase() + ':pdp-specs:' + dl.product.category1 + ':' + dl.product.category2;
            if (typeof (dl.product.supermodel_ids[0]) !== 'undefined')
                return s.prop23 === tempName + ':' + dl.product.supermodel_ids[0].toUpperCase();
            else if (typeof (dl.product.model_ids[0]) !== 'undefined')
                return s.prop23 === tempName + ':' + dl.product.model_ids[0].toUpperCase();
            else if (typeof (dl.product.sku_ids[0]) !== 'undefined')
                return s.prop23 === tempName + ':' + dl.product.sku_ids[0].toUpperCase();
            break;
        case 'subgallery':
            if ((typeof (dl.product.category2) !== 'undefined' && typeof (dl.product.category1) !== 'undefined' && typeof (dl.page.tab_layout) === 'undefined'))
                return s.prop23 === dl.page.section + dl.page.country.toLowerCase() + ':' + dl.page.template + ':' + dl.product.category1 + ':' + dl.product.category2 + ':' + dl.page.tab_name;
            else if (typeof (dl.product.category2) !== 'undefined' && typeof (dl.product.category1) !== 'undefined' && typeof (dl.page.tab_layout) !== 'undefined' && dl.page.tab_layout === 'comparison')
                return s.prop23 === dl.page.section + dl.page.country.toLowerCase() + ':compare:' + dl.page.template + ':' + dl.product.category1 + ':' + dl.product.category2 + ':' + dl.page.tab_name;
            break;
    }
    ;
};
var validateCurrencyCode = function (value, dataLayer){
    var domain = dataLayer.domain;
    return validateCountry(domain, null, value);
};
var validateProp10 = function (value, dataLayer) {
    var dl = dataLayer;
    var s = {};
    s.prop10 = value;
    switch (dl.page.template) {
        case 'home':
            return s.prop10 === dl.page.template;
            break;
        case 'category':
            return s.prop10 === dl.page.template;
            break;
        case 'gallery':
            return s.prop10 === dl.page.template;
            break;
        case 'product_details':
            return s.prop10 === 'pdp';
            break;
        case 'reviews_listing':
            return s.prop10 === dl.page.template;
            break;
        case 'other':
            if (dl.page.template_revision === 'all-electronics')
                return s.prop10 === dl.page.name.replace('other:','');
            else if (dl.page.template_revision === 'locale-selector')
                return s.prop10 === 'other';
            else if (dl.page.template_revision === 'language-selection')
                return s.prop10 === 'language-selection';
            else if (dl.page.template_revision === 'error-default')
                return s.prop10 === dl.page.name.replace('other:','');
            else if (dl.page.template_revision === 'error_page')
                return s.prop10 === dl.page.name.replace('other:','');
            else if (dl.page.template_revision === 'favorites' || dl.page.template_revision.indexOf("Page-Favorites") > -1)
                return  s.prop10 === 'my favorites';
            break;
        case 'out_of_flow':
            return s.prop10 === 'oof';
            break;
        case 'product_tech_specs':
            return s.prop10 === 'pdp:tech-specs';
            break;
        case 'subgallery':
            return s.prop10 === dl.page.template;
            break;
    }
};
//Omniture Variables under test and association with their validation functions
//The property name must match EXACTLY the name of the variable on the page.
//Validation function:
//Input #1: Value to be validated
//Input #2: DataLayer
//Output: TRUE or FALSE

var variablesToTest = [];
variablesToTest['pageName'] = validatePageName;
variablesToTest['prop10'] = validateProp10;
variablesToTest['prop3'] = function(){
    return true;
};
variablesToTest['prop23'] = validateProp23;
variablesToTest['currencyCode'] = validateCurrencyCode;
variablesToTest['account'] = validateReportSuite;

exports.tests = function () {
    var tests = {};
    tests.getHTTPTests = function () {
        return marketingTagsURLs;
    };

    tests.getVariablesToTest = function () {
        return JSON.stringify(Object.keys(variablesToTest));
    };

    tests.ValidateProperty = function (propertyName, propertyValue, dataLayer) {
        if (typeof variablesToTest[propertyName] !== 'undefined') {
            var tmp_result = variablesToTest[propertyName](propertyValue.trim(), dataLayer);
            return tmp_result;
        } else
            return 'n/a';
    };
    return tests;
};